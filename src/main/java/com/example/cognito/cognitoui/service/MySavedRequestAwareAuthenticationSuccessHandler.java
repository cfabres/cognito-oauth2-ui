package com.example.cognito.cognitoui.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

@Service
public class MySavedRequestAwareAuthenticationSuccessHandler  extends SavedRequestAwareAuthenticationSuccessHandler{

	private static final Logger logger = LoggerFactory.getLogger(MySavedRequestAwareAuthenticationSuccessHandler.class);
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
		List<GrantedAuthority> updatedAuthorities = new ArrayList<>(authentication.getAuthorities());
		updatedAuthorities.add(new SimpleGrantedAuthority("ROLE_NEW_ROLE")); //add your role here [e.g., new SimpleGrantedAuthority("ROLE_NEW_ROLE")]

		Authentication newAuth = new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(), updatedAuthorities);

		SecurityContextHolder.getContext().setAuthentication(newAuth);
		
		String username = ((DefaultOidcUser)authentication.getPrincipal()).getAttributes().get("cognito:username").toString();
		String email = ((DefaultOidcUser)authentication.getPrincipal()).getAttributes().get("email").toString();
		List<String> groups = (List<String>) ((DefaultOidcUser)authentication.getPrincipal()).getAttributes().get("cognito:groups");
		Collection<? extends GrantedAuthority> roles = authentication.getAuthorities();
		
		logger.info("username: " + username + "'");
		logger.info("email: " + email + "'");
		logger.info("groups: " + (groups==null?null:groups.toString()) + "'");
		logger.info("roles: " + roles.toString() + "'");

		super.onAuthenticationSuccess(request, response, authentication);
	}

}
