package com.example.cognito.cognitoui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CognitouiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CognitouiApplication.class, args);
	}

}
