package com.example.cognito.cognitoui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	
	@Value("${app.url}")
	private String appUrl;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		 .csrf()
		     .disable()
        .authorizeRequests()
            .antMatchers( "/,/img/**,/css/**,/js/**" ).permitAll()
            .anyRequest().authenticated()
            .and()
        .oauth2Login()
        	.loginPage( this.appUrl + "/oauth2/authorization/" + "cognito-client-1" )
            .permitAll()
            .successHandler( this.authenticationSuccessHandler ) //injection of the listener to save the user
            .and()
        .logout() 
        	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        	.logoutSuccessUrl("/")
            .permitAll();
	}	

	
}
